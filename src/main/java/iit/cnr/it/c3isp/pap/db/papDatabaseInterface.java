package iit.cnr.it.c3isp.pap.db;

public interface papDatabaseInterface {
	
	public boolean createTable();
	
	public boolean addPolicy(String policyId, byte[] policy);
	
	public boolean removePolicy(String policyId);
	
	public byte[] retrievePolicy(String policyId);
	
	public boolean updatePolicy(String policyId, String policy);
	
}
