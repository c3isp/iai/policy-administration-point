package iit.cnr.it.c3isp.pap.db;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import it.cnr.iit.springswagger.restapi.types.PAPParams;

public class PAPDBFunctions implements papDatabaseInterface {
	
	@Value("${spring.datasource.url}")
	private String									dbUrl;
	@Value("${spring.datasource.username}")
	private String									dbUser;
	@Value("${spring.datasource.password}")
	private String									dbPass;
	@Value("${pep.database.table.name}")
	private String									tableName;
	
	private ConnectionSource				connectionSource;
	private Dao<PAPParams, String>	papDao;
	private boolean									isInitialized	= false;
	
	private final static Logger			LOGGER				= LoggerFactory
	    .getLogger(PAPDBFunctions.class);
	
	public PAPDBFunctions() {
	}
	
	public boolean initDb() {
		
		try {
			connectionSource = new JdbcConnectionSource(dbUrl, dbUser, dbPass);
			papDao = DaoManager.createDao(connectionSource, PAPParams.class);
			setInizialized(true);
			
			return getInizialized();
			
		} catch (SQLException e) {
			LOGGER.error("PAP> Error in initializing pap_db!");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	private void refresh() {
		try {
			if (!connectionSource.isOpen(tableName)) {
				connectionSource = new JdbcPooledConnectionSource(dbUrl, dbUser,
				    dbPass);
				papDao = DaoManager.createDao(connectionSource, PAPParams.class);
				setInizialized(true);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			setInizialized(false);
		}
	}
	
	@Override
	public boolean createTable() {
		
		if (!getInizialized()) {
			LOGGER.error("PAP> pap_db not initialized!");
			return false;
		}
		
		try {
			TableUtils.createTableIfNotExists(connectionSource, PAPParams.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("PAP> Unable to create Policies table properly");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Override
	public byte[] retrievePolicy(String policyId) {
		
		if (!getInizialized()) {
			LOGGER.error("PAP> pap_db not initialized!");
			return null;
		}
		
		try {
			refresh();
			List<PAPParams> policyList = papDao.queryBuilder()
			    .selectColumns(PAPParams.POLICY_FIELD).where()
			    .eq(PAPParams.ANALYTIC_ID_FIELD, policyId).query();
			
			if (policyList.size() > 0) {
				return policyList.get(0).getPolicy();
			} else
				return null;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	@Override
	public boolean addPolicy(String policyId, byte[] policy) {
		
		if (!getInizialized()) {
			LOGGER.error("PAP> pap_db not initialized!");
			return false;
		}
		
		refresh();
		
		if (retrievePolicy(policyId) != null) {
			LOGGER.error(
			    "PAP> Entry with analyticId " + policyId + " already present!");
			return false;
		}
		
		PAPParams papParams = new PAPParams();
		papParams.setPolicyId(policyId);
		papParams.setPolicy(policy);
		
		try {
			papDao.create(papParams);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean removePolicy(String policyId) {
		
		if (!getInizialized()) {
			LOGGER.error("PAP> pap_db not initialized!");
			return false;
		}
		
		try {
			refresh();
			DeleteBuilder<PAPParams, String> deleteBuilder = papDao.deleteBuilder();
			deleteBuilder.where().eq(PAPParams.ANALYTIC_ID_FIELD, policyId);
			int rows = deleteBuilder.delete();
			
			LOGGER.info("PAP> Entry with " + policyId + " policy id deleted");
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean updatePolicy(String policyId, String policy) {
		
		if (!getInizialized()) {
			LOGGER.error("PAP> pap_db not initialized!");
			return false;
		}
		
		try {
			refresh();
			UpdateBuilder<PAPParams, String> updateBuilder = papDao.updateBuilder();
			updateBuilder.where().eq(PAPParams.ANALYTIC_ID_FIELD, policyId);
			updateBuilder.updateColumnValue(PAPParams.POLICY_FIELD, policy);
			updateBuilder.update();
			
			LOGGER
			    .info("PAP> Entry with " + policyId + " updated with the new policy");
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean getInizialized() {
		return this.isInitialized;
	}
	
	public void setInizialized(boolean isInizialized) {
		this.isInitialized = isInizialized;
	}
	
}
