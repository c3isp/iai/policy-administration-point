package it.cnr.iit.springswagger.restapi.types;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Policies")
public class PAPParams {
	
	public static final String	ID_FIELD					= "id";
	public static final String	ANALYTIC_ID_FIELD	= "policy_id";
	public static final String	POLICY_FIELD			= "policy";
	
	@DatabaseField(generatedId = true, columnName = ID_FIELD)
	private Integer							id;
	@DatabaseField(canBeNull = false, columnName = ANALYTIC_ID_FIELD)
	private String							policyId;
	@DatabaseField(canBeNull = false, columnName = POLICY_FIELD,
	    dataType = DataType.BYTE_ARRAY)
	private byte[]							policy;
	
	public PAPParams() {
	};
	
	public String getPolicyId() {
		return policyId;
	}
	
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	
	public byte[] getPolicy() {
		return policy;
	}
	
	public void setPolicy(byte[] policy) {
		this.policy = policy;
	}
	
}
