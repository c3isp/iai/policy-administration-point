/**
 * Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.springswagger.restapi.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import iit.cnr.it.c3isp.pap.db.PAPDBFunctions;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

@ApiModel(value = "PAP API",
    description = "These are the APIs to call in order to to store and retrieve policies related to each analytic")
@RestController
@RequestMapping("/v1")
public class PAPServiceImplementation {
	
	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local
	 * path, it can be in the java classpath or set as an environment variable
	 */
	@Value("${setting1}")
	private String					setting1;
	
	@Value("${setting2}")
	private String					setting2;
	
//	@Autowired
//	private RestTemplate		restTemplate;
	
	@Autowired
	private PAPDBFunctions	dbFunctions;
	
	@Bean
	public PAPDBFunctions papDbFunctions() {
		return new PAPDBFunctions();
	}
	
	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory
	    .getLogger(PAPServiceImplementation.class);
	
	@RequestMapping(method = RequestMethod.GET, value = "fetchPolicy")
	@ApiOperation(
	    value = "Call this api in order to get the policy releated to the analytic,\r\n"
	        + "passing as argument the analytic ID")
	public String fetchPolicy(@RequestParam() String policyId) {
		
		byte[] policy;
		String result;
		
		try {
			
			if (!dbFunctions.getInizialized()) {
				dbFunctions.initDb();
			}
			
			if ((policy = dbFunctions.retrievePolicy(policyId)) != null) {
				result = new String(policy);
			} else {
				return "NOT FOUND";
			}
			
			return result;
			
		} catch (Exception e) {
			
			// params.setPolicy("FAILED");
			e.printStackTrace();
			return "EXCEPTION";
		}
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "setPolicy")
	@ApiOperation(
	    value = "Call this api if you want to set the policy for a given analytic,\r\n"
	        + "passing as arguments the policy itself and the policy ID")
	public String setPolicy(@RequestParam() MultipartFile newPolicy,
	    @RequestParam() String policyId) {
		
		try {
			if (!dbFunctions.getInizialized()) {
				dbFunctions.initDb();
			}
			
			dbFunctions.createTable();
			
			byte[] policyInByte;
			policyInByte = newPolicy.getBytes();
			
			if (!dbFunctions.addPolicy(policyId, policyInByte)) {
				return "FAILED";
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "EXCEPTION";
		}
		
		return "CREATED";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "removePolicy")
	@ApiOperation(
	    value = "Call this api if you want to set the policy for a given analytic,\r\n"
	        + "passing as arguments the policy itself and the policy ID")
	public String removePolicy(@RequestParam() String policyId) {
		
		try {
			if (!dbFunctions.getInizialized()) {
				dbFunctions.initDb();
			}
			
			if (!dbFunctions.removePolicy(policyId)) {
				return "FAILED";
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "EXCEPTION";
		}
		
		return "DELETED";
	}
}
